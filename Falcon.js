/**
 *  This js file acts as a Facade for the Falcon ui
 */

import StageManager from "./Stage/StageManager.js";
import Fragment from "./Fragment/Fragment.js";
//import Log from "./Log.js";
import FalconCore from "./core/core.js";

const TAG = "Falcon"

export const MAIN_STAGE_ID = "main";


export default
class Falcon {
    static newStageManager() {
        return new StageManager();
    }

    static StageManager() {
        return StageManager.getInstance();
    }

    /**
     *
     * @param elementId [required]
     * @param html [required]
     * @param callback [optional]
     */
    static fillElementWithHtml(elementId, html, callback) {
        FalconCore.setElementHtmlById(elementId, html, callback);
    }

    static newFragment(id, container) {
        return new Fragment(id, container);
    }
}