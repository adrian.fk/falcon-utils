/*
* @Objective: Handle the process of writing to the HTML page
 */


import {requiredParameter} from "../Tools.js";
import Log from "../Log.js";
import Falcon from "../Falcon.js";

const TAG = "RenderEngine";

function generateMainTag(mainId, contentHtml = "") {
    return `
        <main id="${mainId}" class="${mainId}">${contentHtml}</main>
    `;
}

function setElementContentHtmlById(elementId,
                               elementContentHtml = requiredParameter(TAG,
                                   "elementContentHtml",
                                   "setElementContentById") || "") {

    const element = document.getElementById(elementId);
    if (element) {
        if (elementContentHtml !== "") {
            element.innerHTML = elementContentHtml;
        }
        else {
            Log.d(TAG, "FillElementWithHtml attempted filling element with an empty html string.")
        }
    }
    else {
        Log.d(TAG, "FillElementWithHtml could not find element by that id.")
    }
}

export default
class RenderEngine {

    static getInstance() {
        return new RenderEngine();
    }

    constructor() {
        if (!RenderEngine.INSTANCE) RenderEngine.INSTANCE = this;
        return RenderEngine.INSTANCE;
    }

    init() {
        const stage = document.getElementsByTagName("BODY")[0];
        stage.innerHTML = generateMainTag("main");
        Falcon.StageManager().setStageByID("main");

    }

    setElementHtmlById(elementID, elementHtml) {
        setElementContentHtmlById(elementID, elementHtml);
    }
}