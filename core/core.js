//Facade


import RenderEngine from "./RenderEngine.js";

export default
class FalconCore {
    static start() {
        const renderEngine = RenderEngine.getInstance();
        renderEngine.init();
    }

    static stop() {

    }

    static setElementHtmlById(elementId, elementHtml, callback) {
        const renderEngine = RenderEngine.getInstance();
        renderEngine.setElementHtmlById(elementId, elementHtml);
        if (callback) callback();
    }
}