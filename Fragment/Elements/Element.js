export const TYPE_ID_ELEMENT = 11;
export const TYPE_ID_IMAGE = 22;
export const TYPE_ID_BUTTON = 33;
export const TYPE_ID_MENU = 44;


/**
 * Element
 * Superclass for all insertable elements into a fragment
 **/

export default class Element {

    constructor(elementID) {
        this.background = "#FFFFFF"
        this.typeID = TYPE_ID_ELEMENT;
        this.elementID = elementID;
        this.text = "";
        this.corners = "0px";

        Element.TYPE_ID_ELEMENT = 11;
        Element.TYPE_ID_IMAGE = 22;
        Element.TYPE_ID_BUTTON = 33;
        Element.TYPE_ID_MENU = 44;
    }

    withText(text) {
        this.text = text;
        return this;
    }

    withOnClickListener(listener) {
        this.onClickListener = listener;
        this.registerListener();
    }

    constructInnerHTML(contentHTML = "") {
        if (this.hasListener) {
            this.innerHTML =
                "<div id='" + this.elementID + "' style='cursor: pointer; border-radius: +" + this.corners + ";'>" + contentHTML + "</div>";
        }
        else {
            this.innerHTML = "<div id='" + this.elementID + "' style='border-radius: " + this.corners +";'>" + contentHTML + "</div>";
        }
    }

    registerListener() {
        document.getElementById(this.elementID).onclick = this.onClickListener;
    }

    setText(newText) {
        this.text = newText;
    }

    setCorners(radius) {
        this.corners = radius;
    }

    setWidthHeight(width, height) {
        this.width = width;
        this.height = height;
        this.hasDimensions = true;
    }

    removeDimensions() {
        this.hasDimensions = false;
    }

    getInnerHTML() {
        return this.innerHTML;
    }

    setInnerHTML(newInnerHTML) {
        this.innerHTML = newInnerHTML;
    }

    setOnClickListener(listener) {
        this.onClickListener = listener;
        this.registerListener();
    }
}