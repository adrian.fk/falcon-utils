import Element from "./Element.js";
import {TYPE_ID_IMAGE} from "./Element.js";

export default class Image extends Element{

    constructor(elementID, imgPath) {
        super(elementID);
        this.typeID = TYPE_ID_IMAGE;
        this.imgPath = imgPath;
    }

    setImgPath(imgPath) {
        this.imgPath = imgPath;
    }


    constructInnerHTML() {
        let contentHTML = "";
        if (this.hasDimensions) {
            contentHTML = "<img id='img_" + this.elementID + "' src='" + this.imgPath + "' alt='" + this.text
                + "' width='" + this.width + "' height='" + this.height + "'" + ">";
        }
        else {
            contentHTML = "<img id='img_" + this.elementID + "' src='" + this.imgPath + "' alt='" + this.elementID + "'>";
        }
        super.constructInnerHTML(contentHTML);
    }
}