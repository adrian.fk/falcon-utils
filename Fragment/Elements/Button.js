import Element from "./Element.js";
import {TYPE_ID_BUTTON} from "./Element.js";

export default class Button extends Element {

    constructor(elementID, text) {
        super(elementID);
        this.typeID = TYPE_ID_BUTTON
    }


    constructInnerHTML() {
        let contentHTML = "";
        if(this.hasDimensions) {
            contentHTML = "<button type='button' style='width: " + this.width + "; height: " + this.height + "'>" + this.text + "</button>"
        }
        else {
            contentHTML = "<button type='button'>" + this.text + "</button>"
        }
        super.constructInnerHTML(contentHTML);
    }
}