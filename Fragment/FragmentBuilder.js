import Fragment from "./Fragment";

export default class FragmentBuilder {

    constructor() {
    }

    applyFragmentAsContent(fragment) {
        this.fragmentContent = fragment;
        this.contentHTML = fragment.getInnerHtml();
        this.hasFragmentContent = true;
    }

    make() {

    }
}