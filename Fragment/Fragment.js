import HtmlFetcher from "../HtmlFetcher.js";
import Log from "../Log.js";
import {requiredParameter} from "../Tools.js";

const TAG = "Fragment";

export default class Fragment {

    constructor(fragmentID, container) {
        this.onSpawnListener = function () {};
        this.container = container;

        if (fragmentID) this.fragmentID = fragmentID;
        else {
            throw "can't create fragment without fragment ID"
        }
        this.content = new FragmentContent();
    }

    withBackground(background) {
        this.background = background;
        return this;
    }

    withHtmlFile(filePath, callback = undefined) {
        this.fillWithHtmlFile(filePath, callback);

        return this;
    }

    setBackground(newBackground) {
        this.background = newBackground;
    }

    fillWithHtmlFile(filePath, callback = undefined) {
        const callbackWhenHtmlFileFetched = (html) => {
            this.replaceInnerHtml(html);
            if (callback !== undefined) callback(html);
        }

        const htmlFetcher = new HtmlFetcher();
        htmlFetcher.fetchAsync(
            filePath,
            callbackWhenHtmlFileFetched
        )
    }

    withOnSpawnListener(onSpawnListener) {
        this.onSpawnListener = onSpawnListener;
        return this;
    }

    createFragmentDivWithContentHTML(contentHTML) {
        if (this.onCreate) this.onCreate();
        if (this.background) return `<div id='${this.fragmentID}' class='${this.fragmentID}' style='background: ${this.background}; top: 0; left: 0;'>${contentHTML}</div>`;
        return `<div id='${this.fragmentID}' class='${this.fragmentID}' style='top: 0; left: 0;'>${contentHTML}</div>`;
    }

    setFragmentID(fragmentID) {
        this.fragmentID = fragmentID;
        return this;
    }

    getDomElement() {
        return document.getElementById(this.fragmentID);
    }

    getInnerHtml() {
        return this.content.getInnerHtml();
    }

    replaceInnerHtml(innerHTML) {
        this.content.clear();
        this.content.setInnerHTML(innerHTML);
        return this;
    }

    setFullscreen(fullscreen = false) {
        this.content.setFullscreen(fullscreen);
    }

    getNumContainingFragments() {
        return this.content.getStackSize();
    }

    addFragment(fragment, position) {
        this.content.addFragment(fragment, position);
    }

    onCreate() {
        this.onSpawnListener();
    }

    commit(boolCommitToContainer, callback) {
        if (this.content.hasFragmentAsContent()) {
            this.content.commitFragmentPool();
        }

        if (boolCommitToContainer && !!this.container) {
            let frag = document.getElementById(this.fragmentID);
            if (!frag) {
                //Append div class in within stage
                this.container.insertAdjacentHTML("beforeend", this.createFragmentDivWithContentHTML(this.content.getInnerHtml()));
            }
            else {
                //Target div and replace html
                frag.innerHTML = this.getInnerHtml();
            }
        }

        if (callback) callback();
    }

}
class FragmentContent {

    constructor() {
        this.fullscreen = false;
        this.innerHTML = "";
        this.fragments = new Map();
        this.fragmentIndexToKeyArray = [];
    }

    setInnerHTML(newInnerHTML = requiredParameter(TAG, "newInnerHTML", "setInnerHtml") || "") {
        this.innerHTML = newInnerHTML;
    }

    addFragment(fragment = new Fragment(), position = this.fragmentIndexToKeyArray.length) {
        const posOutOfBounds = () => position < 0 || position > this.fragmentIndexToKeyArray.length;

        if (posOutOfBounds()) position = this.fragmentIndexToKeyArray.length;

        this.fragments.set(fragment.fragmentID, fragment);

        if (position === this.fragmentIndexToKeyArray.length) {
            this.fragmentIndexToKeyArray.push(fragment.fragmentID);
        }
        else  {
            const firstPartOfArray = this.fragmentIndexToKeyArray.slice(0, position - 1);
            const lastPartOfArray = this.fragmentIndexToKeyArray.slice(position - 1, this.fragmentIndexToKeyArray.length);
            firstPartOfArray.push(fragment.fragmentID);
            this.fragmentIndexToKeyArray = firstPartOfArray.concat(lastPartOfArray);
        }
    }

    setFullscreen(fullscreen) {
        this.fullscreen = fullscreen;
    }

    getFragmentById(fragmentId) {
        return this.fragments.get(fragmentId);
    }

    getFragmentByPosition(position) {
        return this.getFragmentByIndex(position - 1);
    }

    getFragmentByIndex(index) {
        return this.fragments.get(this.fragmentIndexToKeyArray[index]);
    }

    hasFragmentAsContent() {
        return this.fragmentIndexToKeyArray.length > 0;
    }

    clear() {
        this.innerHTML = "";
        this.fragments.clear();
        this.fragmentIndexToKeyArray = [];
    }

    getInnerHtml() {
        return this.innerHTML;
    }

    getStackSize() {
        return this.fragmentIndexToKeyArray.length;
    }

    commitFragmentPool() {
        const htmlFullscreenDivTagOpen = "<div style='width: 100%; height:100vh;'>";
        const htmlFullscreenDivTagClose = "</div>";

        let htmlPool = "";

        Log.d(TAG, "Accumulating HTML");
        for (let i = 0; i < this.fragmentIndexToKeyArray.length; i++) {
            Log.d(TAG, "committing fragment #" + i + " to html pool");

            let fragment = this.fragments.get(this.fragmentIndexToKeyArray[i]);
            if (this.fullscreen) {
                htmlPool += "\n" + htmlFullscreenDivTagOpen + fragment.getInnerHtml() + htmlFullscreenDivTagClose;
            }
            else {
                htmlPool += "\n" + fragment.getInnerHtml();
            }
        }
        this.innerHTML = htmlPool;
    };
}