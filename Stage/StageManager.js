import HtmlFetcher from "../HtmlFetcher.js";
import Log from "../Log.js";
import {requiredParameter} from "../Tools.js";
import Stage from "./Stage.js";
import FalconCore from "../core/core.js";
/**
 *  Stage Manager serves the purpose of managing a full stage.
 *  with this, it is referring to whole page as a stage.
 */

const TAG = "StageManager";


export default class StageManager {

    static getInstance(stageID) {
        return new StageManager(stageID);
    }

    constructor(stageID) {
        if (!StageManager.instance) {
            StageManager.instance = this;
            this.cssReferences = [];
            this.importCSS("./FalconUtils/Falcon.css");
        }
        this.stage = new Stage(stageID);
        return StageManager.instance;
    }

    setStageByID(stageID = requiredParameter(TAG, "stageID", "setStageByID")) {
        if (stageID !== undefined) this.stage.setStage(stageID);
    }

    setStage(stage = requiredParameter(TAG, "stage", "setStage") || new Stage()) {
        this.stage = stage;
    }

    scrollToTop() {
        this.stage.scrollToTop();
    }

    /**
     *
     * @param destinationHtmlPath
     * @param destinationCssPath
     * @param callback: Callback after inner HTML is set. can be undefined, if no callback is desired.
     */
    navigateToHtml(destinationHtmlPath = undefined, destinationCssPath = undefined, callback = undefined) {
        if (destinationCssPath !== undefined) this.importCSS(destinationCssPath);
        if (destinationHtmlPath !== undefined) {
            Log.d(TAG, "Navigating to: " + destinationHtmlPath + " with stylesheet: " + destinationCssPath);

            const sm = this;
            const htmlFetcher = new HtmlFetcher();

            sm.toggleFade();

            setTimeout(
                function () {
                    htmlFetcher.fetchAsync(
                        destinationHtmlPath,
                        html => {
                            sm.setStageHTML(html);
                            sm.scrollToTop();
                            sm.toggleFade();
                            if (callback !== undefined) callback(html);
                        }
                    )
                },
                300
            )
        }
    }

    /**
     *  To toggle CSS fade animation.
     *      Requirements: facilities in CSS to function
     */
    toggleFade() {
        this.stage.stage.classList.toggle("fade");
    }

    setStageHTML(contentHTML, callback = undefined, scrollToTop = false) {
        const container_div = '<div id="_container" style="width: 100%; height: 100vh; top: 0; left: 0;">';
        const container_div_close = ' </div>';
        this.stage.setStageHtml(contentHTML);
        if (scrollToTop) this.scrollToTop();
        if (callback !== undefined) callback();
    }

    importCSS(ref) {
        const header = document.getElementById("header");
        if (ref !== undefined && !this.cssReferences.includes(ref, 0)) {
            this.cssReferences.push(ref);
            header.insertAdjacentHTML('beforeend', '\n' + '<link rel="stylesheet" href="' + ref + '" type="text/css">');
        }
    }

    getDomStage() {
        this.stage.getDomStage();
    }

    /**
     *
     * @param elementId [required]
     * @param html [required]
     * @param callback [optional]
     */
    fillElementWithHtml(
        elementId = requiredParameter(TAG, "elementId", "fillElementWithHtml") || "",
        html = requiredParameter(TAG, "html", "fillElementWithHtml") || "",
        callback) {

        FalconCore.setElementHtmlById(elementId, html, callback);
    }
}